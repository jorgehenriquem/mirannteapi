<?php

namespace App\Http\Controllers;
use App\Models\Imoveis;
use App\Models\Cidades;
use App\Models\ImovelFoto;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;


Class imovel_fotos extends Model
{
    
}

class ImoveisController extends Controller
{


    public function getCidades()
        {
            $cities=cidades::all();
            return response()->json(['cities'=>$cities]);
        }
    
    public function getImoveis($id)
        {
            $findObject=imoveis::find($id);
            $ddw= imoveis::find($id)->id;
            $findPhotos=imovel_fotos::where('imovel_id', $ddw)->get();
            return response()->json(['imovel'=>$findObject, 'photos'=>$findPhotos]);
        }   



    public function index()
        {
            
            //return View ('api');
           return include ('doc/index.html');


        }
}