<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/api', function () use ($router) {
    return include ('doc/index.html');
});


$router->get('/api/imoveis/{id}', 'ImoveisController@getImoveis');
/**
 * @api {get} /api/imoveis/{id} Busca de Imoveis
 * @apiName MirantteAPI
 * @apiDescription Busca Imovel pelo ID
 * @apiGroup Imoveis
 * @apiParam (Parameter) {integer} [id]  Id do imovel e também Codigo do Imovel.
 * @apiParam (Parameter) {String} [endereco_cep]  CEP do Imovel .
 * @apiParam (Parameter) {String} [endereco_logradouro]  Logradouro do Imovel.
 * @apiParam (Parameter) {String} [endereco_numero]  Numero do Imovel.
 * @apiParam (Parameter) {String} [endereco_complemento]  Complemento do Imovel.
 * @apiParam (Parameter) {String} [endereco_bairro] Bairro do Imovel.
 * @apiParam (Parameter) {String} [endereco_cidade]  Referente ao cidade do imovel.
 * @apiParam (Parameter) {String} [endereco_uf]  Estado do Imovel.
 * @apiParam (Parameter) {float} [endereco_map_lat]  Latitude do Imovel.
 * @apiParam (Parameter) {float} [endereco_map_long]  Longitude do Imovel.
 * @apiParam (Parameter) {tinyInt} [dormitorios]  Quantidades de dormitorios que o Imovel possui.
 * @apiParam (Parameter) {tinyInt} [suites]   Quantidades de suites que o Imovel possui.
 * @apiParam (Parameter) {tinyInt} [wc]  Quantidades de banheiros que o Imovel possui.
 * @apiParam (Parameter) {tinyInt} [salas]   Quantidades de salas que o Imovel possui.
 * @apiParam (Parameter) {tinyInt} [vagas]   Quantidades de vagas de garagem que o Imovel possui.
 * @apiParam (Parameter) {float} [area_util] Area Util do Imovel.
 * @apiParam (Parameter) {float} [area_comun] Area Comun do Imovel.
 * @apiParam (Parameter) {float} [area_construida] Area Construida do Imovel.
 * @apiParam (Parameter) {float} [area_total] Area Total do Imovel.
 * @apiParam (Parameter) {float} [area_terreno] Area do terreno.
 * @apiParam (Parameter) {String} [metragem_terreno]  Numero do Imovel.
 * @apiParam (Parameter) {decimal} [valor_venda]  Valor de venda do imovel.
 * @apiParam (Parameter) {decimal} [valor_locacao]  Valor de Aluguel do imovel.
 * @apiParam (Parameter) {decimal} [valor_condominio]  Valor do condominio do imovel.
 * @apiParam (Parameter) {String} [valor_iptu]  Valor do IPTU do imovel.
 * @apiParam (Parameter) {tinyInt} [permuta_aceita]  Se o imovel aceita permulta.
 * @apiParam (Parameter) {text} [descricao]  Descricao completa do imovel.
 * @apiParam (Parameter) {integer} [agencia_id]  Id referente a agencia onde esse imovel foi publicado.
 * @apiParam (Parameter) {String} [created_at]  Registro referente a quando o imovel foi criado.
 * @apiParam (Parameter) {String} [updated_at]  Registro referente a quando o imovel foi editado.
 * @apiParam (Photos) {integer} [id]  Id da foto.
 * @apiParam (Photos) {String} [arquivo]  Nome do arquivo da imagem.
 * @apiParam (Photos) {tinyInt} [indice]  Indice da foto.
 * @apiParam (Photos) {String} [descricao]  Descrição da foto.
 * @apiParam (Photos) {integer} [imovel_id]  Numero referente ao id do imovel pertencente a foto.


 @apiSuccessExample Success:
 *     HTTP/1.1 200 OK
 *     {
 *       data: {
        "id": 75023,
        "endereco_cep": "02765040",
        "endereco_logradouro": "VITO OSVALDO SAPONARA",
        "endereco_numero": "90",
        "endereco_complemento": " comple",
        "endereco_bairro": "Jardim Cachoeira",
        "endereco_cidade": "São Paulo",
        "endereco_uf": "SP",
        "endereco_map_lat": null,
        "endereco_map_long": null,
        "dormitorios": 3,
        "suites": 1,
        "wc": 1,
        "salas": 1,
        "vagas": 2,
        "area_util": 160,
        "area_comun": null,
        "area_construida": 160,
        "area_total": null,
        "area_terreno": 160,
        "metragem_terreno": " ",
        "valor_venda": "300000.00",
        "valor_locacao": null,
        "valor_condominio": null,
        "valor_iptu": null,
        "permuta_aceita": 0,
        "descricao": "ÓTIMO SOBRADO! SÃO 3 DORMITÓRIOS SENDO 1 SUÍTE, SALA, COZINHA, QUINTAL, 2 VAGAS.",
        "agencia_id": 1,
        "created_at": "2010-02-10 00:00:00",
        "updated_at": "2012-04-04 18:24:40",
 *     }
              {
*               photos: [
        {
                "id": 27,
                "arquivo": "001_01411_00005_29283_2010012216494670.jpg",
                "indice": 1,
                "descricao": "VITO OSVALDO SAPONARA",
                "imovel_id": 75023,
        },
        {
                "id": 45,
                "arquivo": "001_00000_00000_70980015_2010020409392290.jpg",
                "indice": 2,
                "descricao": "VARANDA",
                "imovel_id": 75023,
        }
                        ]
    }
 */
